# Dockerfile

::: tip ARIKETA
`nginx`, `wordpress` eta `mysql`/`mariadb` irudien `Dockerfile`-a aztertu dezagun.
:::


* `FROM`
* `LABEL`
* `ENV`
* `RUN`
* `EXPOSE`
* `STOPSIGNAL`
* `CMD`
* `VOLUME`
* `COPY`
* `ENTRYPOINT`
* `USER`
* `WORKDIR`


::: tip ARIKETA
`wordpress` irudiaren `tag` ezberdinak aztertu: `apache` vs `php-fpm`
:::
