# docker-compose.yml


[WordPress](https://hub.docker.com/_/wordpress/) irudi ofizialean dagoen `docker-compose.yml` tuneatuko degu.


``` yaml
version: '3.1'
volumes:
  db-data: {}
  www-data: {}

services:
  nginx:
    image: nire-nginx
    # build: nginx/
    restart: always
    volumes:
    - www-data:/var/www/html
    ports:
      - 8080:80
    environment:
      SERVER_NAME: nireweb.test
  wordpress:
    image: wordpress:4.9.6-fpm-alpine
    restart: always
    environment:
      WORDPRESS_DB_PASSWORD: example
      # WORDPRESS_DB_USER=root
      # WORDPRESS_DB_HOST: mysql
      # WORDPRESS_DB_NAME=wordpress
    volumes:
    - www-data:/var/www/html
  mysql:
    image: mariadb:10
    restart: always
    volumes:
    - db-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: example
      # MYSQL_DATABASE: wordpress
      # MYSQL_USER: user
      # MYSQL_PASSWORD: example
```


## WordPress-entzat nginx prestatzen

Nginx irudi bat prestatuko dugu WordPress-ekin erabiltzeko, [WP nginx konfigurazio eredua](https://codex.wordpress.org/Nginx#General_WordPress_rules) erabiliko dugu. Ingurune aldagaiak erabiliko ditugu nginx zerbitzua konfiguratzeko.

### www.conf

``` nginx
# WordPress single site rules.
# Designed to be included in any server {} block.
# Upstream to abstract backend connection(s) for php
upstream php {
        server wordpress:9000;
}

server {
        ## Your website name goes here.
        server_name DOMEINUA;
        ## Your only path reference.
        root /var/www/html;
        ## This should be in your http block and if it is, it's not needed here.
        index index.php;

        location = /favicon.ico {
                log_not_found off;
                access_log off;
        }

        location = /robots.txt {
                allow all;
                log_not_found off;
                access_log off;
        }

        location / {
                # This is cool because no php is touched for static content.
                # include the "?$args" part so non-default permalinks doesn't break when using query string
                try_files $uri $uri/ /index.php?$args;
        }

        location ~ \.php$ {
                #NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
                include fastcgi.conf;
                fastcgi_intercept_errors on;
                fastcgi_pass php;
                fastcgi_buffers 16 16k;
                fastcgi_buffer_size 32k;
              #  fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
              #  fastcgi_param DOCUMENT_ROOT $realpath_root;
        }

        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                expires max;
                log_not_found off;
        }
}

```
### Dockerfile

``` docker
FROM nginx:alpine

COPY www.conf /etc/nginx/conf.d/www.conf

COPY docker-entrypoint.sh /
# RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
```

### docker-entrypoint.sh

``` bash
#! /bin/sh
set -e

sed -i 's/DOMEINUA/'"${DOMEINUA:-froga.test}"'/' /etc/nginx/conf.d/www.conf

exec nginx -g "daemon off;"
```
