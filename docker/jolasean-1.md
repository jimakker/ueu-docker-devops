# Jolasean 1

[https://hub.docker.com/](https://hub.docker.com/) esploratu, ea gustoko zer edo zer topatu eta martxan jartzen dugun!

::: tip BONUS
Datubasea behar duen software/aplikazio batek saltsa emango digu!
:::

## Gauza berriak

Aukera, parametro ... berriak azalduko zaizkigu, garrantzitsuenetako batzuk:

* `-e`: (`--env`) Ingurune aldagaiak ezartzeko (`--env-file`)
* `--restart`: Berrasiarazteko politika ([docs](https://docs.docker.com/engine/reference/run/#restart-policies---restart))
* `--rm`: komandoa exekutatzen amaitzean edukiontzia (eta *volume* anonimoak) ezabatu
* `--link`: edukiontziak *lotu*
* `--volumes-from`
* ...

Aukera ugari daude! Guztiak [hemen](https://docs.docker.com/engine/reference/commandline/cli/).


::: tip ARIKETA
[WordPress](https://hub.docker.com/_/wordpress/) jarriko dugu martxan.
:::


## Trolleatzen

Docker-en boterea ausnartzeko ariketatxoa.

```
docker run -it -v /etc/hosts:/etc/lol ubuntu sh
```
