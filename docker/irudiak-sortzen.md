# Irudiak sortzen

## Dockerfile

`Dockerfile`-a da docker irudiak sortzeko erabiltzen den errezeta.

Exekutatzen den direktorioa erabiltzen du *testuinguru* gisa.

``` docker
FROM nginx:alpine
COPY index.html /usr/share/nginx/html
# COPY index.html /usr/share/nginx/html/index.html
# COPY . /usr/share/nginx/html
```

## docker build

```
docker build -t IZENA:TAG Dockerfile_fitxategia
```

Aurreko `Dockerfile`-a sortzeko:

```
docker build -t nire-nginx .
```

::: tip TIP
Puntu (`.`) horrek gauden direktorioan `Dockerfile` bilatezko adierazten dio. Beste bide bat adierazi diezaiokegu nahai izanez gero.
:::

Tag bat ezartzeko:

```
docker build -t nire-nginx:nire-tag .
docker build -t nire-nginx:0.1.0 .
```

::: tip ARIKETA
*Jarri martxan* sortu berri dugun irudia.
:::

## .dockerignore

`COPY` / `ADD` egiterakoan kontuan ez hartzeko fitxategi/bideak definitzeko erabiltzen den fitxategia (*.gitignore* antzera).

## docker push

Irudiak erregistrora bidaltzeko erabiltzen da.

::: tip ARIKETA
GitLab-en irudi erregistrora igoko dugu irudia.
:::
